let authentication = require("./authentication");
 const googleapis = require('googleapis');
var sheets = googleapis.sheets('v4');

function getData(auth) {
  return new Promise((resolve) => {
  sheets.spreadsheets.get({
    auth: auth,
    spreadsheetId: '17OnvgT_ftHUcKtS-JaXAWTc5jcN09EFFlJd2kLEdMcc',
    includeGridData: false,
    range: [], //Change Sheet1 if your worksheet's name is something else
  }, (err, response) => {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    } 
    var gsheetArray = response.sheets;
    var sheetTitles = [];
    for(let i = gsheetArray.length -1 ; i >= gsheetArray.length - 1; i--){
      sheetTitles.push(gsheetArray[i].properties.title);
    }
    console.log("got all sheets ------------")
    resolve({auth : auth, data : sheetTitles})
  }); 
});
};
var loadTime = [];
var metric = [];
function getSheetData(res) {
  var sheetNames = res.data;
  for(let i in sheetNames){
    console.log("inside for ", sheetNames[i])
    sheets.spreadsheets.values.get({
      auth: res.auth,
      spreadsheetId: '17OnvgT_ftHUcKtS-JaXAWTc5jcN09EFFlJd2kLEdMcc',
      range: sheetNames[i], //Change Sheet1 if your worksheet's name is something else
      majorDimension:'ROWS',
    }, (err, response) => {
      if (err) {
        console.log('The API returned an error: ' + err);
        return;
      } 
      var rows = response.values;
      var breakpoint = [];
        for(let i in rows){
          if(rows[i].length == 0){
            breakpoint.push(i);
          }
        }
        
        var metadata = [];
        for(let i = parseInt(breakpoint[2])+1; i < breakpoint[3]; i++){
            metadata.push(rows[i]);
        }
        // console.log(metadata);
        console.log(get_gSheetDate(metadata));

        var resultdata = [];

        for(let i = parseInt(breakpoint[3])+1; i < rows.length; i++){
            resultdata.push(rows[i]);
        }

        // console.log(resultdata);

        var lookinto = [];
        
        for(let i = 0; i<resultdata[0].length;i++){
          if(resultdata[0][i] === 'Metric' || resultdata[0][i] === 'Load time (ms)'){
            lookinto.push({name: resultdata[0][i], index: i});
          }
        }
        // console.log(lookinto);
        if(metric.length === 0){
          getMetricName(lookinto,resultdata)
        }
        loadTime.push(getLoadTime(lookinto,resultdata));
        //console.log(loadTime);

     });
  }
};

function get_gSheetDate(metadata){
 metadata.map(function(i){
    if(i[1].match(/Timestamp:/)){
      let time = i[1].split(":");
      _day = time[1].split(" ").splice(2,3).join(" ");
    }
  });
 return _day;
}


function getMetricName(lookinto, resultdata){
  var index = '';
  lookinto.map(function(data){
    if(data.name == 'Metric'){
      index = data.index;
    }
  })
  console.log("index",index);
  for(let i = 2; i<resultdata.length;i++){
    metric.push(resultdata[i][index]);
  }
  //console.log(metric);
}

function getLoadTime(lookinto, resultdata){
  var index = '';
  var temp = [];
  lookinto.map(function(data){
    if(data.name == 'Load time (ms)'){
      index = data.index;
    }
  })
  // console.log("index",index);
  for(let i = 2; i<resultdata.length;i++){
    temp.push(resultdata[i][index]);
  }
    // console.log(temp);

  return temp;
}


 
authentication.authenticate()
.then((auth)=>{
    return getData(auth).then((res) => {return res;})      
})
.then((res)=>getSheetData(res));
 